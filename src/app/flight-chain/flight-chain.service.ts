import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {AcrisFlight} from '../acris-schema/AcrisFlight';
import {catchError, mergeMap, tap} from 'rxjs/operators';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {NGXLogger} from 'ngx-logger';
import {FlightChainData} from "../acris-schema/AcrisFlightHistoryFromBlockchain";
import {environment} from "../../environments/environment";
import {AuthService} from '../auth/auth.service';
import {FlightHistoryResponse} from './FlightHistoryResponse';

@Injectable({
  providedIn: 'root'
})
export class FlightChainService {

  constructor(private http: HttpClient,
              private _logger: NGXLogger,
              private authService: AuthService) {
  }

  private flightURL:string = environment.flightChainAPI;  // URL to web api
  private userName:string = environment.userName;
  private userPass:string = environment.userPass;

  // private flightURL:string = 'http://fchainapi-mia.dev.blockchainsandbox.aero/flightChain/';

  /** GET one flight from the server */
  getFlight(flightKey: String): Observable<AcrisFlight | HttpErrorResponse> {
    return this.http.get<AcrisFlight>(this.flightURL + flightKey)
      .pipe(
        tap(flight => this._logger.debug('fetched flight')),
        catchError(this.handleError('getFlight', null))
      );
  }
  /** GET history of updates for a flight from the server */
  getFlightHistory(flightKey: String): Observable<FlightHistoryResponse[]| HttpErrorResponse> {
    return this.authService.login(this.userName, this.userPass)
      .pipe(mergeMap(authResponse => {
        console.log('I got the JWT: ' + authResponse.jwt);
        const httpOptions = {
          headers: new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + authResponse.jwt
          })
        };
        return this.http.get<FlightHistoryResponse[]>(this.flightURL + flightKey+'/history', httpOptions)
          .pipe(
            tap(flight => this._logger.debug('fetched flight history')),
            catchError(this.handleError('getFlightHistory', []))
          );
      }));
  }

  getTransaction(transactionId: string) {
    return this.authService.login(this.userName, this.userPass)
      .pipe(mergeMap(authResponse => {
        console.log('I got the JWT: ' + authResponse.jwt);
        const httpOptions = {
          headers: new HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + authResponse.jwt
          })
        };

        return this.http.get<any>(this.flightURL +'transaction/'+transactionId, httpOptions)
          .pipe(
            tap(flight => this._logger.debug('fetched transactionInfo')),
            catchError(this.handleError('getTransaction', null))
          );
      }));
  }

  /**
   * Handle the error, and return empty result to let app continue
   *
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      this._logger.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this._logger.info(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(error);
    };
  }



}
